import { mount, shallow, render } from "enzyme";
import React, { Component } from "react";
import HelloWorld from "../HelloWorld";
it("renders correctly", () => {
    const wrapper = shallow(
      <HelloWorld/>
    );
    expect(wrapper).toMatchSnapshot();
  });