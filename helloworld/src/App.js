import React,{Component} from 'react';
import './App.css';
import HelloWorld from './HelloWorld'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
class App extends Component {
  render()
  {
  return (
    <div className="App">
      <HelloWorld/>
    </div>
  );
  }
  
}

export default App;
